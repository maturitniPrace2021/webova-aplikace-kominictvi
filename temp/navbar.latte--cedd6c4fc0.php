<?php

use Latte\Runtime as LR;

/** source: template\navbar.latte */
final class Templatecedd6c4fc0 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
</head>
<body>
  <nav class="navbar">

    <a href="#" class="toggle-button">
      <span class="bar"></span>
      <span class="bar"></span>
      <span class="bar"></span>
    </a>
    <div class="navbar-links">
      <ul>
        <li><a id="sectionUvod" href="http://localhost/Kominictvi-master/index.php#Uvod">Úvod</a></li>
        <li><a id="sectionServices" href="http://localhost/Kominictvi-master/index.php#Sluzby">Služby</a></li>
        <li><a id="sectionOmně" href="http://localhost/Kominictvi-master/index.php#O mne">O mně</a></li>
        <li><a id="sectionFotogalerie" href="http://localhost/Kominictvi-master/index.php#Fotogalerie">Fotogalerie</a></li>
        <li><a id="sectionKontakty" href="http://localhost/Kominictvi-master/index.php#Kontakty">Kontakty</a></li>
      </ul>
    </div>
  </nav>
</body>
</html>
';
		return get_defined_vars();
	}

}
