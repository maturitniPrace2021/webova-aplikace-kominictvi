<?php

use Latte\Runtime as LR;

/** source: ../template/vystavba.latte */
final class Template5190fb85ed extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../style/dist/sluzby.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Výstavba</title>
</head>

<body>

';
		$this->createTemplate('navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 15 */;
		echo '
  <div class="nadpis">
    <h1>Výstavba komínu</h1>
  </div>

  <section class="one">

  <div class="container1">

    <div class="img1">
      <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($vystavba["0"]["2"])) /* line 26 */;
		echo '" alt="vystavba1">
    </div>

    <div class="aboutText">
      <h3>';
		echo LR\Filters::escapeHtmlText($vystavba["0"]["0"]) /* line 30 */;
		echo '</h3>
      <p>';
		echo LR\Filters::escapeHtmlText($vystavba["0"]["1"]) /* line 31 */;
		echo '</p>
    </div>
  </div>

</section>

  <section class="two">

  <div class="container2">

    <div class="aboutText2">
      <h3>';
		echo LR\Filters::escapeHtmlText($vystavba["1"]["0"]) /* line 42 */;
		echo '</h3>
      <p>';
		echo LR\Filters::escapeHtmlText($vystavba["1"]["1"]) /* line 43 */;
		echo '</p>
    </div>

    <div class="img2">
      <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($vystavba["1"]["2"])) /* line 47 */;
		echo '" alt="vystavba2">
    </div>

  </div>

</section>

<section class="three">

  <div id="Kontakty">

    <div class="formText">
      <h4>Máte dotaz nebo chcete, abych se Vám ozval? Zanechte mi tady Váš kontakt</h4>
    </div>
    <div class="contact">
      <div class="contactText">
        <p>Jakub Verner</p>
        <p>Boží Dar 134</p>
        <p>362 62 Boží Dar</p>
        <p>Tel: +420 603 498 814</p>
        <p>E-mail: muj-kominik@centrum.cz</p>
        <p>IČ: 46855211</p>
      </div>

      <div class="contactForm">

        <form method="POST" action="" id="contactForm" name="contactForm">

          <input type="text" name="name" id="name" size="30" placeholder="Jméno a příjmení" required><br>
          <input type="tel" name="phone" id="phone" size="30" placeholder="Tel. číslo" required><br>
          <input type="email" name="email" id="email" size="30" placeholder="E-mail" required><br>
          <textarea name="note" name="note" id="note" cols="36" rows="8" placeholder="Poznámka"></textarea><br>
          <input type="number" name="verify" id="verify" placeholder="2+2=" required><br>
          <div class="button">
          <button class="button">Odeslat</button>
        </div>
        </form>
      </div>
    </div>


    <div class="map">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2542.6326989675686!2d12.919302015624998!3d50.41068247946992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a0a4fba5241b87%3A0x54f5a57abeb57e8a!2zQm_FvsOtIERhciAxMzQsIDM2MyAwMSBCb8W-w60gRGFy!5e0!3m2!1scs!2scz!4v1612799908604!5m2!1scs!2scz"
        width="100%" height="350vh" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>
    </div>

</section>

<footer>
  <div class="copyright">
    <p>Kominictví Verner 2021 - Všechna práva vyhrazena</p>
  </div>
  <div class="admin">
    <a href="login.html">Admin</a>
  </div>
</footer>

  <script>
   const toggleButton = document.getElementsByClassName(\'toggle-button\')[0]
const navbarLinks = document.getElementsByClassName(\'navbar-links\')[0]

toggleButton.addEventListener(\'click\', () => {
  navbarLinks.classList.toggle(\'active\')
})
  </script>





</body>

</html>
';
		return get_defined_vars();
	}

}
