<?php

use Latte\Runtime as LR;

/** source: ../template/administrace.latte */
final class Template76a677225d extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="../style/dist/administrace.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Administrace</title>
</head>
<body>
';
		$this->createTemplate('navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 13 */;
		echo '  <div class="vyber">
    <h1>Vybrat úpravu</h1>
    <a href="seznamRezervaci.php"><button class="button">Seznam rezervací</button></a>
    <a href="pridatClanek.php"><button class="button">Přidat článek</button></a>
    <a href="upravitClanek.php"><button class="button">Upravit článek</button></a>
    <a href="smazatClanek.php"><button class="button">Smazat článek</button></a>
    <a href="logout.php"><button class="button">Odhlásit se</button></a>
  </div>
</body>
</html>
';
		return get_defined_vars();
	}

}
