<?php

use Latte\Runtime as LR;

/** source: ../template/seznamRezervaci.latte */
final class Template9949c52dcd extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="../style/dist/seznamRezervaci.css">
  <script src="../js_src/functions.js"></script>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Seznam rezervací</title>
</head>
<body>
';
		$this->createTemplate('navbar-admin.latte', $this->params, 'include')->renderToContentType('html') /* line 15 */;
		echo '  <h1>Seznam zapsaných rezervací v databázi</h1>
  <table id="reservations">
    <tr id="nadpisy">
      <th>Jméno a Přijmení</th>
      <th>E-mail</th>
      <th>Datum Rezervace</th>
      <th>Datum Vytvoření Rezervace</th>
    </tr>
';
		$iterations = 0;
		foreach ($rezervace as $item) /* line 24 */ {
			echo '      <tr>
        <td>';
			echo LR\Filters::escapeHtmlText($item[1]) /* line 26 */;
			echo '</td>
        <td>';
			echo LR\Filters::escapeHtmlText($item[2]) /* line 27 */;
			echo '</td>
        <td>';
			echo LR\Filters::escapeHtmlText($item[3][0]) /* line 28 */;
			echo ' - ';
			echo LR\Filters::escapeHtmlText($item[3][2]) /* line 28 */;
			echo ' - ';
			echo LR\Filters::escapeHtmlText($item[3][1]) /* line 28 */;
			echo '</td>
        <td>';
			echo LR\Filters::escapeHtmlText($item[4]) /* line 29 */;
			echo '</td>
      </tr>
';
			$iterations++;
		}
		echo '  </table>

</body>
</html>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['item' => '24'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
