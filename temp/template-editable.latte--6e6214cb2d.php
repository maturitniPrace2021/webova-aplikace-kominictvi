<?php

use Latte\Runtime as LR;

/** source: ../template/editable.latte */
final class Template6e6214cb2d extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="../style/dist/clanky.css">
  <script src="../js_src/functions.js"></script>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <script src="https://cdn.tiny.cloud/1/qh10rgarbegu8afz316lty9q9d3vot8mwrlatkg0gy1agg4f/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
  tinymce.init({
    selector: \'#editor\'
  });
  </script>
  <title>Kominictví Verner - Úprava článku</title>
</head>

<body>
';
		$this->createTemplate('navbar-admin.latte', $this->params, 'include')->renderToContentType('html') /* line 21 */;
		echo '
  <div class="form">
    <h1>Začít upravovat</h1>
    <h2 hidden>';
		echo LR\Filters::escapeHtmlText($kategorie) /* line 25 */;
		echo ' - ';
		echo LR\Filters::escapeHtmlText($id) /* line 25 */;
		echo '</h2>
    <form action="successUprava.php" method="post" enctype="multipart/form-data">
      <input type="hidden" name="articleID" value="';
		echo LR\Filters::escapeHtmlAttr($id) /* line 27 */;
		echo '">
      <input type="hidden" name="kategorie" value="';
		echo LR\Filters::escapeHtmlAttr($kategorie) /* line 28 */;
		echo '">
      <label for="articleName">Název článku:</label><br>
      <textarea name="nadpis" rows="8" cols="80">';
		echo LR\Filters::escapeHtmlText($radek[0][0]) /* line 30 */;
		echo '</textarea><br>
      <label for="articleContent">Obsah článku:</label><br>
      <textarea id="editor" name="popis" rows="8" cols="80">';
		echo LR\Filters::escapeHtmlText($radek[0][1]) /* line 32 */;
		echo '</textarea>
      <label for="articlePic">Foto článku:</label><br>
      <div>
        <img src="../img/';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($radek[0][2])) /* line 35 */;
		echo '">
      </div>
      <input type="file" id="img" name="img"><br>

      <button class="button" type="submit" name="upravit">Upravit Článek</button>
    </form>
  </div>

  <!-- <div class="vypis">
  <p>';
		echo LR\Filters::escapeHtmlComment($statusMsg) /* line 44 */;
		echo '</p>
</div> -->
</body>

</html>
';
		return get_defined_vars();
	}

}
