<?php

use Latte\Runtime as LR;

/** source: ../template/vystavba.latte */
final class Template865da4e5d6 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../style/dist/sluzby.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Výstavba</title>
</head>

<body>

';
		$this->createTemplate('navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 16 */;
		echo '
  <div class="nadpis">
    <h1>Výstavba komínu</h1>
  </div>
';
		for ($i = 0;
		$i < $pocetVystavba;
		$i++) /* line 21 */ {
			if ($idVystavba % 2 == 0) /* line 22 */ {
				echo '
  <section class="left">

    <div class="container1">

      <div class="img1">
        <img src="../img/';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($vystavba["$i"]["2"])) /* line 29 */;
				echo '" alt="vystavba1">
      </div>

      <div class="aboutText">
        <h3>';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($vystavba["$i"]["0"])) /* line 33 */;
				echo '</h3>
        <p id="justify">';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($vystavba["$i"]["1"])) /* line 34 */;
				echo '</p>
      </div>
    </div>

  </section>

';
				$idVystavba++ /* line 40 */;
			}
			else /* line 41 */ {
				echo '
  <section class="right">

    <div class="container2">

      <div class="aboutText2">
        <h3>';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($vystavba["$i"]["0"])) /* line 48 */;
				echo '</h3>
        <p id="justify">';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($vystavba["$i"]["1"])) /* line 49 */;
				echo '</p>
      </div>

      <div class="img2">
        <img src="../img/';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($vystavba["$i"]["2"])) /* line 53 */;
				echo '" alt="vystavba2">
      </div>

    </div>

  </section>

';
				$idVystavba++ /* line 60 */;
			}
			echo "\n";
		}
		echo "\n";
		$this->createTemplate('footer.latte', $this->params, 'include')->renderToContentType('html') /* line 65 */;
		echo '
  </body>
  </html>
';
		return get_defined_vars();
	}

}
