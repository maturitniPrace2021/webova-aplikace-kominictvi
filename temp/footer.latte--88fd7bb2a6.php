<?php

use Latte\Runtime as LR;

/** source: template\footer.latte */
final class Template88fd7bb2a6 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
</head>
<body>

  <section class="six">

    <div id="Kontakty">

      <div class="formText">
        <h4>Máte dotaz nebo chcete, abych se Vám ozval? Zanechte mi tady Váš kontakt</h4>
      </div>
      <div class="contact">
        <div class="contactText">
          <p>';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["0"]) /* line 18 */;
		echo '</p>
          <p>';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["1"]) /* line 19 */;
		echo '</p>
          <p>';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["2"]) /* line 20 */;
		echo '</p>
          <p>Tel: ';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["3"]) /* line 21 */;
		echo '</p>
          <p>E-mail: ';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["4"]) /* line 22 */;
		echo '</p>
          <p>IČ: ';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["5"]) /* line 23 */;
		echo '</p>
        </div>

        <div class="contactForm">

          <form method="POST" action="src/napistenam.php" id="contactForm" name="contactForm">

            <input type="text" name="name" id="name" size="30" placeholder="Jméno a příjmení" required><br>
            <input type="tel" name="phone" id="phone" size="30" placeholder="Tel. číslo" required><br>
            <input type="email" name="email" id="email" size="30" placeholder="E-mail" required><br>
            <textarea name="note" name="note" id="note" cols="36" rows="8" placeholder="Poznámka"></textarea><br>
            <input type="number" name="verify" id="verify" placeholder="2+2=" required><br>
            <div class="button">
              <button class="button">Odeslat</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="map">
      <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2542.6326989675686!2d12.919302015624998!3d50.41068247946992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a0a4fba5241b87%3A0x54f5a57abeb57e8a!2zQm_FvsOtIERhciAxMzQsIDM2MyAwMSBCb8W-w60gRGFy!5e0!3m2!1scs!2scz!4v1612799908604!5m2!1scs!2scz"
      width="100%" height="350vh" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
      tabindex="0"></iframe>
    </div>

  </section>


  <footer>
    <div class="copyright">
      <p>Kominictví Verner 2021 - Všechna práva vyhrazena</p>
    </div>
    <div class="admin">
      <a href="src/login.php">Admin</a>
    </div>
  </footer>

</body>
</html>
';
		return get_defined_vars();
	}

}
