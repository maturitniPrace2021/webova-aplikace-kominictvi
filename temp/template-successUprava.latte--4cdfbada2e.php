<?php

use Latte\Runtime as LR;

/** source: ../template/successUprava.latte */
final class Template4cdfbada2e extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
      <link rel="stylesheet" href="../style/dist/clanky.css">
      <script src="../js_src/functions.js"></script>
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
      <script src="https://cdn.tiny.cloud/1/qh10rgarbegu8afz316lty9q9d3vot8mwrlatkg0gy1agg4f/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <title>Kominictví Verner - Úprava článku | úspěšná</title>
  </head>
<body>
<p>';
		echo LR\Filters::escapeHtmlText($statusMsg) /* line 15 */;
		echo '</p>
</body>

</html>
';
		return get_defined_vars();
	}

}
