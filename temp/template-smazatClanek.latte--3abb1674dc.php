<?php

use Latte\Runtime as LR;

/** source: ../template/smazatClanek.latte */
final class Template3abb1674dc extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style/dist/clanky.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
    <title>Kominictví Verner - Smazání článku</title>
</head>

<body>

    <h1>Smazat článek</h1>

    <form action="" method="post">
        <label for="name">Název:</label><br>
        <input type="text" name="name"><br>
        <label for="id">ID článku: </label><br>
        <input type="number" name="id"><br>
        <label for="category">Kategorie: </label><br>
        <input type="text" name="category"><br>
        <button class="button" href="">Smazat článek</button>
    </form>

</body>

</html>';
		return get_defined_vars();
	}

}
