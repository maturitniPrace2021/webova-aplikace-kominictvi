<?php

use Latte\Runtime as LR;

/** source: ../template/upravitClanek.latte */
final class Templateee77c98c89 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style/dist/clanky.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
    <title>Kominictví Verner - Úprava článku</title>
</head>

<body>

    <h1>Upravit článek</h1>

    <form action="" method="post">
        <label for="name">Název:</label><br>
        <input type="text" name="name"><br>
        <label for="id">ID článku: </label><br>
        <input type="number" name="id"><br>
        <label for="category">Kategorie: </label><br>
        <input type="text" name="category"><br>
        <label for="articleContent">Obsah:</label><br>
        <textarea name="articleContent" rows="10" cols="60"></textarea><br>
        <label for="img">Obrázek:</label><br>
        <input type="text" name="img"><br>
        <button class="button" href="">Upravit článek</button>
    </form>

</body>

</html>';
		return get_defined_vars();
	}

}
