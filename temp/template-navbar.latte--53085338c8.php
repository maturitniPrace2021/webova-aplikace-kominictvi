<?php

use Latte\Runtime as LR;

/** source: ../template/navbar.latte */
final class Template53085338c8 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<nav class="navbar">

  <a href="#" class="toggle-button">
    <span class="bar"></span>
    <span class="bar"></span>
    <span class="bar"></span>
  </a>
  <div class="navbar-links">
    <ul>
      <li><a id="sectionUvod" href="https://www.muj-kominik.cz/index.php#Uvod">Úvod</a></li>
      <li><a id="sectionServices" href="https://www.muj-kominik.cz/index.php#Sluzby">Služby</a></li>
      <li><a id="sectionOmně" href="https://www.muj-kominik.cz/index.php#Omne">O mně</a></li>
      <li><a id="sectionFotogalerie" href="https://www.muj-kominik.cz/index.php#Fotogalerie">Fotogalerie</a></li>
      <li><a id="sectionKontakty" href="https://www.muj-kominik.cz/index.php#Kontakty">Kontakty</a></li>
    </ul>
  </div>
</nav>
';
		return get_defined_vars();
	}

}
