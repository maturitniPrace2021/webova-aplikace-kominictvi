<?php

use Latte\Runtime as LR;

/** source: ../template/smazatClanek.latte */
final class Template7422674520 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="../style/dist/clanky.css">
  <script src="../js_src/functions.js"></script>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Smazání článku</title>
</head>

<body>
';
		$this->createTemplate('navbar-admin.latte', $this->params, 'include')->renderToContentType('html') /* line 15 */;
		echo '
  <h1>Smazat článek</h1>

  <form action="successSmazani.php" method="POST" enctype="multipart/form-data">
    <select id="kategorie" name="kategorie" onchange="vyberKategorie(this.value)">
      <option value="default">Výběr kategorie</option>
      <option value="cisteni">Čištění</option>
      <option value="sanace">Sanace</option>
      <option value="vystavba">Výstavba</option>
    </select><br>
    <label for=\'articleName\'>Název článku:</label><br>
    <select id="nazevClanku" name=\'articleID\'>
      <option value=\'\'>Výběr článku</option>
    </select><br>
    <button class="button" type="submit" name="smazatClanek">Smazat článek</button>
  </form>
</body>

</body>

</html>
';
		return get_defined_vars();
	}

}
