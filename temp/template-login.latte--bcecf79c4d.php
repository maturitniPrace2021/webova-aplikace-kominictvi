<?php

use Latte\Runtime as LR;

/** source: ../template/login.latte */
final class Templatebcecf79c4d extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style/dist/login.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
    <title>Kominictví Verner - Login</title>
</head>

<body>

';
		$this->createTemplate('../template/navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 15 */;
		echo '

    <div class="login">
        <div class="loginText">
            <h1>Kominictví Verner - Login</h1>
        </div>

        <div class="form">
            <form action="login.php" method="POST">
                <label for="username">Uživatelské jméno</label><br>
                <input type="text" name="username" id="username" value="';
		echo LR\Filters::escapeHtmlAttr($username) /* line 26 */;
		echo '"><br>
                <label for="password">Heslo</label><br>
                <input type="password" name="password" id="password" value=""><br>
                <button class="button">Přihlásit se</button>
            </form>
        </div>
    </div>

</body>

</html>
';
		return get_defined_vars();
	}

}
