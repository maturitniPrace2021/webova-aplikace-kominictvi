<?php

use Latte\Runtime as LR;

/** source: ../template/navbar-admin.latte */
final class Template795c7bf0ad extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
</head>
<body>
  <nav class="navbar">
    <a href="#" class="toggle-button">
      <span class="bar"></span>
      <span class="bar"></span>
      <span class="bar"></span>
    </a>
    <div class="navbar-links">
      <ul>
        <li><a href="https://www.muj-kominik.cz/src/login.php">Administrace</a></li>
        <li><a href="https://www.muj-kominik.cz/src/seznamRezervaci.php">Seznam rezervací</a></li>
        <li><a href="https://www.muj-kominik.cz/src/pridatClanek.php">Přidat článek</a></li>
        <li><a href="https://www.muj-kominik.cz/src/upravitClanek.php">Upravit článek</a></li>
        <li><a href="https://www.muj-kominik.cz/src/smazatClanek.php">Smazat článek</a></li>
        <li><a href="https://www.muj-kominik.cz/src/logout.php">Odhlásit se</a></li>
      </ul>
    </div>
  </nav>
</body>
</html>
';
		return get_defined_vars();
	}

}
