<?php

use Latte\Runtime as LR;

/** source: template/index.latte */
final class Template8510d79c74 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style/dist/style.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner</title>
  <script src="../js_src/functions.js" async></script>

</head>

<body>

  <div id="Uvod" class="header">
    <video id="myVideo" width="100%" height="10%" autoplay muted loop><source src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["0"])) /* line 18 */;
		echo '" type="video/mp4" alt="video of smoke"></video>
    <div class="nadpis">
      <h1>Kominictví Verner</h1>
      <p>Vaše krušnohorské kominictví</p>
      <p>';
		echo LR\Filters::escapeHtmlText($kontakt["0"]["3"]) /* line 22 */;
		echo '</p>
    </div>
  </div>

';
		$this->createTemplate('navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 26 */;
		echo '
  <section class="one">
    <div id="Sluzby" class="containers" style="cursor: pointer;">

      <a href="src/cisteni.php" class="cisteni">
        <div>
          <h2>';
		echo LR\Filters::escapeHtmlText($sluzba[0][0]) /* line 33 */;
		echo '</h2>
          <p>';
		echo LR\Filters::escapeHtmlText($sluzba[0][1]) /* line 34 */;
		echo '</p>
        </div>
      </a>

      <a href="src/sanace.php" class="sanace">
        <div>
          <h2>';
		echo LR\Filters::escapeHtmlText($sluzba[1][0]) /* line 40 */;
		echo '</h2>
          <p>';
		echo LR\Filters::escapeHtmlText($sluzba[1][1]) /* line 41 */;
		echo '</p>
        </div>
      </a>

      <a href="src/vystavba.php" class="vystavba">
        <div>
          <h2>';
		echo LR\Filters::escapeHtmlText($sluzba[2][0]) /* line 47 */;
		echo '</h2>
          <p>';
		echo LR\Filters::escapeHtmlText($sluzba[2][1]) /* line 48 */;
		echo '</p>
        </div>
      </a>
    </div>
  </section>

  <section class="two">

    <div class="calendarText">
      <h2>Najděte si termín, který Vám vyhovuje</h2>
    </div>
    <div class="responsive-iframe-container">
      <div class="calendar-desktop">
        <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FPrague&amp;src=a29taW5pay52ZXJuZXJAZ21haWwuY29t&amp;src=NjJva2luMWFoN2I5am5iN2lrbDBpajlqdXNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=Y3MuY3plY2gjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%23039BE5&amp;color=%23AD1457&amp;color=%237986CB&amp;showTitle=1&amp;showNav=1&amp;showPrint=0&amp;showTabs=1&amp;showCalendars=0&amp;showTz=0&amp;title=Obsazenost%20term%C3%ADn%C5%AF" style="border-width:0" width="860" height="600" frameborder="0" scrolling="no"></iframe>
      </div>
      <div class="calendar-mobile">
        <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FPrague&amp;src=NjJva2luMWFoN2I5am5iN2lrbDBpajlqdXNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=Y3MuY3plY2gjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%23AD1457&amp;color=%237986CB&amp;mode=MONTH" style="border:solid 1px #777" width="700" height="600" frameborder="0" scrolling="no"></iframe>
      </div>
    </div>

  </section>

  <section class="four">

    <div id="O mne" class="aboutMe">
      <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($omne["0"]["2"])) /* line 73 */;
		echo '" alt="chimney-sweeper on a roof" width="10%" height="10%">

      <div class="aboutText">
        <h3>O mně</h3>
        <p>';
		echo LR\Filters::escapeHtmlText($omne["0"]["0"]) /* line 77 */;
		echo '<p>
        <p>';
		echo LR\Filters::escapeHtmlText($omne["0"]["1"]) /* line 78 */;
		echo '</p>
      </div>
    </div>
  </section>

    <section class="five">

      <div id="Fotogalerie" class="gallery">

        <div class="gallery-item">
          <img class="gallery-image" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["1"])) /* line 88 */;
		echo '" alt="chimney-sweeper cleaning a stove">
        </div>

        <div class="gallery-item">
          <img class="gallery-image" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["2"])) /* line 92 */;
		echo '" alt="chimney-sweeper taking out soot from a chimney">
        </div>

        <div class="gallery-item">
          <img class="gallery-image" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["3"])) /* line 96 */;
		echo '" alt="chimney-sweeper repairing a boiler ">
        </div>

        <div class="gallery-item">
          <img class="gallery-image" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["4"])) /* line 100 */;
		echo '" alt="chimney-sweeper fixing a chimney on a roof">
        </div>

        <div class="gallery-item">
          <img class="gallery-image" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["5"])) /* line 104 */;
		echo '" alt="chimney-sweeper sweeping chimney on a roof in a city">
        </div>

        <div class="gallery-item">
          <img class="gallery-image" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($foto["6"])) /* line 108 */;
		echo '" alt="chimney-sweeper standing on a roof">
        </div>

      </div>

    </section>

';
		$this->createTemplate('footer.latte', $this->params, 'include')->renderToContentType('html') /* line 115 */;
		echo '
</body>
</html>
';
		return get_defined_vars();
	}

}
