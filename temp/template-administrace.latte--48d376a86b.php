<?php

use Latte\Runtime as LR;

/** source: ../template/administrace.latte */
final class Template48d376a86b extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../style/dist/administrace.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Administrace</title>
</head>

<body>

';
		$this->createTemplate('navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 15 */;
		echo '
  <div class="vyber">

    <h1>Vybrat úpravu</h1>
    <a href="pridatClanek.php"><button class="button">Přidat článek</button></a>
    <a href="upravitClanek.php"><button class="button">Upravit článek</button></a>
    <a href="smazatClanek.php"><button class="button">Smazat článek</button></a>
    <a href="logout.php"><button class="button">Odhlásit se</button></a>


  </div>


</body>
</html>
';
		return get_defined_vars();
	}

}
