<?php

use Latte\Runtime as LR;

/** source: ../template/cisteni.latte */
final class Template44662a903d extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../style/dist/sluzby.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@300&display=swap" rel="stylesheet">
  <title>Kominictví Verner - Čištění</title>
</head>

<body>

';
		$this->createTemplate('navbar.latte', $this->params, 'include')->renderToContentType('html') /* line 18 */;
		echo '
  <div class="nadpis">
    <h1>Čištění a údržba komínu</h1>
  </div>

';
		for ($i = 0;
		$i < $pocetCisteni;
		$i++) /* line 24 */ {
			if ($idCisteni % 2 == 0) /* line 25 */ {
				echo '
  <section class="left">

    <div class="container1">

      <div class="img1">
        <img src="../img/';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($cisteni["$i"]["2"])) /* line 32 */;
				echo '" alt="cisteni1">
      </div>

      <div class="aboutText">
        <h3>';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($cisteni["$i"]["0"])) /* line 36 */;
				echo '</h3>
        <p id="justify">';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($cisteni["$i"]["1"])) /* line 37 */;
				echo '</p>
      </div>
    </div>

  </section>

';
				$idCisteni++ /* line 43 */;
			}
			else /* line 44 */ {
				echo '
  <section class="right">
    <div class="container2">

      <div class="aboutText2">
        <h3>';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($cisteni["$i"]["0"])) /* line 50 */;
				echo '</h3>
        <p id="justify">';
				echo LR\Filters::escapeHtmlText(($this->filters->striphtml)($cisteni["$i"]["1"])) /* line 51 */;
				echo '</p>
      </div>

      <div class="img2">
        <img src="../img/';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($cisteni["$i"]["2"])) /* line 55 */;
				echo '" alt="cisteni2">
      </div>

    </div>

  </section>

';
				$idCisteni++ /* line 62 */;
			}
			echo "\n";
		}
		echo "\n";
		$this->createTemplate('footer.latte', $this->params, 'include')->renderToContentType('html') /* line 67 */;
		echo '

</body>

</html>
';
		return get_defined_vars();
	}

}
