Webová aplikace   kominictví

Vytvořte webovou aplikaci pro prezentaci kominictví, která bude fungovat i pro online rezervaci služeb. Aplikace bude obsahovat administrační část, pomocí níž bude možné kompletně administrovat obsah stránek. Administrátor bude mít možnost vidět i rezervované termíny v sekci, která se zobrazí po přihlášení. 

 Ondřej Verner
 
    Responzivní UX designe pro PC i mobilní zařízení 

    Vlastní grafický návrh dle UX designe pro PC i mobilní zařízení  

    Vytvoření šablon prezenční části webu s využitím systému Latte 

    Nastylování prezenční části webu s využitím CSS preprocesoru 

    Vytvoření prezentační části administrace aplikace  

    Skripty pomocí JS  
    

Jonáš vysocký

    příprava DB modelu a realizace fyzické databáze v MySQL 

    příprava knihovny objektů pro realizaci backendu v PHP 

    realizace kompletního backendu aplikace v PHP 

    načítání obsahu stránek z DB 

    online rezervační systém objednávek 

    propojení rezervace s Google calendar  

    online blog 

    reference s fotogalerií 

    kompletní administrace s autentizací přístupu přes DB 
