# Webová aplikace - Kominictví
Vytvořte webovou aplikaci pro prezentaci kominictví, která bude fungovat i pro online rezervaci služeb. Aplikace bude obsahovat administrační část, pomocí níž bude možné kompletně administrovat obsah stránek. Administrátor bude mít možnost vidět i rezervované termíny v sekci, která se zobrazí po přihlášení.

Adresa URL na webovou aplikaci: https://www.muj-kominik.cz/
Adresa URL na správu: https://www.muj-kominik.cz/src/administrace.php

Přístup do správy:
Jméno: admin
Heslo: rakousko1
