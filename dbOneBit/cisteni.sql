-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:40
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `cisteni`
--

CREATE TABLE `cisteni` (
  `id` int(100) NOT NULL,
  `nadpis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(1024) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `cisteni`
--

INSERT INTO `cisteni` (`id`, `nadpis`, `popis`, `foto`) VALUES
(1, 'Proč čistit komín?', 'Komín. Srdce vašeho domu. Nevhodně udržovaný komín může být příčinou nejen špatné účinnosti vašeho spotřebiče paliv, ale též může zapříčinit zahoření objektu či dokonce otravu obyvatel objektu špatně odváděnými spalinami.  Spaliny vznikající při spalování paliva je nutné rychle a bezpečně odvézt komínem do volného ovzduší.   Zanesený komínový průduch toto neumožňuje. Na vnitřních stěnách komína se usazují saze, které v některých případech mohou vytvářet vznětlivý a potenciálně velmi nebezpečný povlak.\r\nTakto zanesený komín je potřeba pravidelně čistit.  Tím se výrazně eliminuje riziko zahoření.', 'cisteni1.jpg'),
(2, 'Kdy komín vyčistit?', 'Správně vyčištěný komín má též nezanedbatelný vliv na spotřebu paliva a tím i na správnou účinnost  spotřebiče paliv.  Čistý komín vám ušetří palivo a tím i peníze. Četnost čištění  komínů na tuhá paliva doporučuji 3x - 4x ročně.  Komíny na plynná paliva 1x ročně.', 'cisteni2.jpg');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `cisteni`
--
ALTER TABLE `cisteni`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `cisteni`
--
ALTER TABLE `cisteni`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
