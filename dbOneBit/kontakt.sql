-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:40
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `kontakt`
--

CREATE TABLE `kontakt` (
  `id` int(1) NOT NULL,
  `JmenoPrijmeni` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `Ulice` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `PSC` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `Telefon` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `Email` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `ico` varchar(8) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `kontakt`
--

INSERT INTO `kontakt` (`id`, `JmenoPrijmeni`, `Ulice`, `PSC`, `Telefon`, `Email`, `ico`) VALUES
(1, 'Jakub Verner', 'Boží Dar 134', '362 62 Boží Dar', '+420 603 498 814', 'muj-kominik@centrum.cz', '46855211');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `kontakt`
--
ALTER TABLE `kontakt`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
