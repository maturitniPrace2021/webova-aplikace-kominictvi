-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:39
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `omne`
--

CREATE TABLE `omne` (
  `id` int(1) NOT NULL,
  `osobe` varchar(512) COLLATE utf8_czech_ci NOT NULL,
  `kominictvi` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `omne`
--

INSERT INTO `omne` (`id`, `osobe`, `kominictvi`, `foto`) VALUES
(1, 'Jmenuji se Jakub Verner. Kominíkem jsem již více než čtvrt století. Za tuto dobu mám mnoho zkušeností nejen s čištěním komínů, ale též s jejich sanací a výstavbou. Zkušenosti mám rozšířené i o mnohaletou kominickou praxi v německy mluvících zemích.', 'Na této práci mě nejvíc baví výhledy ze střechy. Každý je totiž jedinečný. Nic se nevyrovná čištění komínu s výhledem na hory. Správně fungující komín = fungující vytápění.', '../img/about_me.jpg');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `omne`
--
ALTER TABLE `omne`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `omne`
--
ALTER TABLE `omne`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
