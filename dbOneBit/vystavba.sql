-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:38
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `vystavba`
--

CREATE TABLE `vystavba` (
  `id` int(100) NOT NULL,
  `nadpis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(512) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `vystavba`
--

INSERT INTO `vystavba` (`id`, `nadpis`, `popis`, `foto`) VALUES
(1, 'Komín - nezbytnost každého domu', 'Každý dům potřebuje komín. Pokud nemáte tepelné čerpadlo. Nabízíme dodávku a montáž lehkých nerezových třívrstvých komínových systémů. Tyto stavebnicové komíny je možné montovat vně i uvnitř stavby.', 'vystavba1.jpg'),
(2, 'Výstavba komínu', 'Velkou předností je jejich váha, komín o vnitřním průměru 150mm a výšce 10m váží okolo 60kg. Lze ho tedy  pověsit na stěnu na speciální základový díl.  Tyto komíny jsou vhodné pro všechny druhy paliv a můžou být dodány v různé povrchové úpravě.\r\nJejich montáž je rychlá a čistá. Většinou je takovýto komín během jediného dne plně funkční.', 'vystavba2.jpg');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `vystavba`
--
ALTER TABLE `vystavba`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `vystavba`
--
ALTER TABLE `vystavba`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
