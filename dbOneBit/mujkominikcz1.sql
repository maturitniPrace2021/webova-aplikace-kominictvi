-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:43
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `captcha`
--

CREATE TABLE `captcha` (
  `hodnota` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `captcha`
--

INSERT INTO `captcha` (`hodnota`) VALUES
(4);

-- --------------------------------------------------------

--
-- Struktura tabulky `cisteni`
--

CREATE TABLE `cisteni` (
  `id` int(100) NOT NULL,
  `nadpis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(1024) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `cisteni`
--

INSERT INTO `cisteni` (`id`, `nadpis`, `popis`, `foto`) VALUES
(1, 'Proč čistit komín?', 'Komín. Srdce vašeho domu. Nevhodně udržovaný komín může být příčinou nejen špatné účinnosti vašeho spotřebiče paliv, ale též může zapříčinit zahoření objektu či dokonce otravu obyvatel objektu špatně odváděnými spalinami.  Spaliny vznikající při spalování paliva je nutné rychle a bezpečně odvézt komínem do volného ovzduší.   Zanesený komínový průduch toto neumožňuje. Na vnitřních stěnách komína se usazují saze, které v některých případech mohou vytvářet vznětlivý a potenciálně velmi nebezpečný povlak.\r\nTakto zanesený komín je potřeba pravidelně čistit.  Tím se výrazně eliminuje riziko zahoření.', 'cisteni1.jpg'),
(2, 'Kdy komín vyčistit?', 'Správně vyčištěný komín má též nezanedbatelný vliv na spotřebu paliva a tím i na správnou účinnost  spotřebiče paliv.  Čistý komín vám ušetří palivo a tím i peníze. Četnost čištění  komínů na tuhá paliva doporučuji 3x - 4x ročně.  Komíny na plynná paliva 1x ročně.', 'cisteni2.jpg');

-- --------------------------------------------------------

--
-- Struktura tabulky `fotogalerie`
--

CREATE TABLE `fotogalerie` (
  `id` int(1) NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `fotogalerie`
--

INSERT INTO `fotogalerie` (`id`, `foto`) VALUES
(1, '../img/video2_1.mp4'),
(2, '../img/foto1.jpg'),
(3, '../img/foto2.jpg'),
(4, '../img/foto3.jpg'),
(5, '../img/foto4.jpg'),
(6, '../img/foto5.jpg'),
(7, '../img/foto5.jpg');

-- --------------------------------------------------------

--
-- Struktura tabulky `kontakt`
--

CREATE TABLE `kontakt` (
  `id` int(1) NOT NULL,
  `JmenoPrijmeni` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `Ulice` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `PSC` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `Telefon` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `Email` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `ico` varchar(8) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `kontakt`
--

INSERT INTO `kontakt` (`id`, `JmenoPrijmeni`, `Ulice`, `PSC`, `Telefon`, `Email`, `ico`) VALUES
(1, 'Jakub Verner', 'Boží Dar 134', '362 62 Boží Dar', '+420 603 498 814', 'muj-kominik@centrum.cz', '46855211');

-- --------------------------------------------------------

--
-- Struktura tabulky `login`
--

CREATE TABLE `login` (
  `id` int(1) NOT NULL,
  `username` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', '$2y$10$KC7kT4Z20smfq31WGvtp9OOV4RnsM9Lxkw98rMjsyr.3.iU/eVEda');

-- --------------------------------------------------------

--
-- Struktura tabulky `napistenam`
--

CREATE TABLE `napistenam` (
  `id` int(100) NOT NULL,
  `osoba` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `telefon` varchar(18) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `poznamka` varchar(512) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `omne`
--

CREATE TABLE `omne` (
  `id` int(1) NOT NULL,
  `osobe` varchar(512) COLLATE utf8_czech_ci NOT NULL,
  `kominictvi` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `omne`
--

INSERT INTO `omne` (`id`, `osobe`, `kominictvi`, `foto`) VALUES
(1, 'Jmenuji se Jakub Verner. Kominíkem jsem již více než čtvrt století. Za tuto dobu mám mnoho zkušeností nejen s čištěním komínů, ale též s jejich sanací a výstavbou. Zkušenosti mám rozšířené i o mnohaletou kominickou praxi v německy mluvících zemích.', 'Na této práci mě nejvíc baví výhledy ze střechy. Každý je totiž jedinečný. Nic se nevyrovná čištění komínu s výhledem na hory. Správně fungující komín = fungující vytápění.', '../img/about_me.jpg');

-- --------------------------------------------------------

--
-- Struktura tabulky `rezervace`
--

CREATE TABLE `rezervace` (
  `id` int(11) NOT NULL,
  `JmenoPrijmeni` varchar(64) COLLATE utf8mb4_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8mb4_czech_ci NOT NULL,
  `datum` varchar(16) COLLATE utf8mb4_czech_ci NOT NULL,
  `datumVytvoreni` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `sanace`
--

CREATE TABLE `sanace` (
  `id` int(100) NOT NULL,
  `nadpis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(512) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `sanace`
--

INSERT INTO `sanace` (`id`, `nadpis`, `popis`, `foto`) VALUES
(1, 'Co je to sanace a kdy se provádí?', 'Když stávající komín vykazuje opotřebení způsobené stárnutím nebo již nevyhovuje požadavkům moderního topení, je nutná jeho sanace.\r\nPři provozu moderních topení u starých domovních komínů jinak hrozí nebezpečí prosakování, protože průřez komína je většinou příliš velký pro nízké teploty spalin a obvodové zdivo komína je kvůli kondenzátu, který vzniká při spalování a obsahuje kyselinu, masivně poškozené.', 'sanace1.jpg'),
(2, 'Jak se provádí sanace?', 'Sanace komína se provádí zpravidla umístěním výfukové trubky z ušlechtilé oceli nebo z polypropylenu skrz hlavici komína. Výběr výfukového systému závisí na druhu topeniště, dimenzování by ale měla bezpodmínečně provést odborná firma.', 'sanace2.jpg');

-- --------------------------------------------------------

--
-- Struktura tabulky `sluzby`
--

CREATE TABLE `sluzby` (
  `id` int(3) NOT NULL,
  `nazev` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(256) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `sluzby`
--

INSERT INTO `sluzby` (`id`, `nazev`, `popis`) VALUES
(1, 'Čištění a údržba', 'Provádíme pravidelné čištění komínů a kouřovodů pro všechny druhy paliv. Také se zabýváme čištěním kotlů, kamen a krbů na tuhá paliva.'),
(2, 'Sanace', 'Sanace stávajících komínů je prováděna za pomoci vložek z nerezové oceli pro tuhá paliva, pro plynové kondenzační kotle z nerezové oceli či plastu.'),
(3, 'Výstavba', 'Výstavba třívrstvých komínů z nerezové oceli vedených po fasádě či vnitřkem budovy. Předností těchto komínů je jejich nízká hmotnost. Tyto komíny jsou určeny pro všechny druhy paliv.');

-- --------------------------------------------------------

--
-- Struktura tabulky `vystavba`
--

CREATE TABLE `vystavba` (
  `id` int(100) NOT NULL,
  `nadpis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(512) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `vystavba`
--

INSERT INTO `vystavba` (`id`, `nadpis`, `popis`, `foto`) VALUES
(1, 'Komín - nezbytnost každého domu', 'Každý dům potřebuje komín. Pokud nemáte tepelné čerpadlo. Nabízíme dodávku a montáž lehkých nerezových třívrstvých komínových systémů. Tyto stavebnicové komíny je možné montovat vně i uvnitř stavby.', 'vystavba1.jpg'),
(2, 'Výstavba komínu', 'Velkou předností je jejich váha, komín o vnitřním průměru 150mm a výšce 10m váží okolo 60kg. Lze ho tedy  pověsit na stěnu na speciální základový díl.  Tyto komíny jsou vhodné pro všechny druhy paliv a můžou být dodány v různé povrchové úpravě.\r\nJejich montáž je rychlá a čistá. Většinou je takovýto komín během jediného dne plně funkční.', 'vystavba2.jpg');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `cisteni`
--
ALTER TABLE `cisteni`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `fotogalerie`
--
ALTER TABLE `fotogalerie`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Klíče pro tabulku `napistenam`
--
ALTER TABLE `napistenam`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `omne`
--
ALTER TABLE `omne`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `rezervace`
--
ALTER TABLE `rezervace`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `sanace`
--
ALTER TABLE `sanace`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `sluzby`
--
ALTER TABLE `sluzby`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `vystavba`
--
ALTER TABLE `vystavba`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `cisteni`
--
ALTER TABLE `cisteni`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `fotogalerie`
--
ALTER TABLE `fotogalerie`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pro tabulku `kontakt`
--
ALTER TABLE `kontakt`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pro tabulku `login`
--
ALTER TABLE `login`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pro tabulku `napistenam`
--
ALTER TABLE `napistenam`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `omne`
--
ALTER TABLE `omne`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pro tabulku `rezervace`
--
ALTER TABLE `rezervace`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `sanace`
--
ALTER TABLE `sanace`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `sluzby`
--
ALTER TABLE `sluzby`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `vystavba`
--
ALTER TABLE `vystavba`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
