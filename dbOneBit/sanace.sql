-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:38
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `sanace`
--

CREATE TABLE `sanace` (
  `id` int(100) NOT NULL,
  `nadpis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(512) COLLATE utf8_czech_ci NOT NULL,
  `foto` varchar(32) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `sanace`
--

INSERT INTO `sanace` (`id`, `nadpis`, `popis`, `foto`) VALUES
(1, 'Co je to sanace a kdy se provádí?', 'Když stávající komín vykazuje opotřebení způsobené stárnutím nebo již nevyhovuje požadavkům moderního topení, je nutná jeho sanace.\r\nPři provozu moderních topení u starých domovních komínů jinak hrozí nebezpečí prosakování, protože průřez komína je většinou příliš velký pro nízké teploty spalin a obvodové zdivo komína je kvůli kondenzátu, který vzniká při spalování a obsahuje kyselinu, masivně poškozené.', 'sanace1.jpg'),
(2, 'Jak se provádí sanace?', 'Sanace komína se provádí zpravidla umístěním výfukové trubky z ušlechtilé oceli nebo z polypropylenu skrz hlavici komína. Výběr výfukového systému závisí na druhu topeniště, dimenzování by ale měla bezpodmínečně provést odborná firma.', 'sanace2.jpg');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `sanace`
--
ALTER TABLE `sanace`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `sanace`
--
ALTER TABLE `sanace`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
