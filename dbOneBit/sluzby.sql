-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 12:38
-- Verze serveru: 5.7.30
-- Verze PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mujkominikcz1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `sluzby`
--

CREATE TABLE `sluzby` (
  `id` int(3) NOT NULL,
  `nazev` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(256) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `sluzby`
--

INSERT INTO `sluzby` (`id`, `nazev`, `popis`) VALUES
(1, 'Čištění a údržba', 'Provádíme pravidelné čištění komínů a kouřovodů pro všechny druhy paliv. Také se zabýváme čištěním kotlů, kamen a krbů na tuhá paliva.'),
(2, 'Sanace', 'Sanace stávajících komínů je prováděna za pomoci vložek z nerezové oceli pro tuhá paliva, pro plynové kondenzační kotle z nerezové oceli či plastu.'),
(3, 'Výstavba', 'Výstavba třívrstvých komínů z nerezové oceli vedených po fasádě či vnitřkem budovy. Předností těchto komínů je jejich nízká hmotnost. Tyto komíny jsou určeny pro všechny druhy paliv.');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `sluzby`
--
ALTER TABLE `sluzby`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `sluzby`
--
ALTER TABLE `sluzby`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
